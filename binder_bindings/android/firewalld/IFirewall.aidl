// Copyright 2016 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package android.firewalld;

@utf8InCpp
interface IFirewall {
  void PunchTcpHole(int port, String iface);
  void PunchUdpHole(int port, String iface);
  void PlugTcpHole(int port, String iface);
  void PlugUdpHole(int port, String iface);
  void RequestVpnSetup(in String[] usernames, String iface);
  void RemoveVpnSetup(in String[] usernames, String iface);
}
