// Copyright 2016 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef FIREWALLD_BINDER_CLIENT_FIREWALL_H_
#define FIREWALLD_BINDER_CLIENT_FIREWALL_H_

#include <memory>
#include <string>
#include <vector>

#include <binderwrapper/binder_wrapper.h>

namespace firewalld {

class Firewall {
 public:
  Firewall() = default;
  virtual ~Firewall() = default;

  static std::unique_ptr<Firewall> Connect(
      android::BinderWrapper* binder_wrapper);

  virtual bool PunchTcpHole(int16_t port, const std::string& iface) = 0;
  virtual bool PunchUdpHole(int16_t port, const std::string& iface) = 0;
  virtual bool PlugTcpHole(int16_t port, const std::string& iface) = 0;
  virtual bool PlugUdpHole(int16_t port, const std::string& iface) = 0;
  virtual bool RequestVpnSetup(const std::vector<std::string>& usernames,
                               const std::string& iface) = 0;
  virtual bool RemoveVpnSetup(const std::vector<std::string>& usernames,
                              const std::string& iface) = 0;

 private:
  DISALLOW_COPY_AND_ASSIGN(Firewall);
};

}  // namespace firewalld

#endif /* FIREWALLD_BINDER_CLIENT_FIREWALL_H_ */
